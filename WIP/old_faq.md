---
title: FAQ
permalink: /faq/
author: Antenore Gatta
layout: single
---

### Background {#faq---background}


*How did Remmina start?*

    Back when netbooks had lackluster resolution, and didn't come with Linux|GNU preinstalled.
    Netbook resolution did not allow scrolling when connecting to any other desktop. Though moving the window was possible by pressing Alt, that meant no captured WM key bindings, and no Alt+Tab in the remote desktop.
    In fullscreen mode, the default Debian RDP client occupied all virtual desktops, providing no control to do other things, with RDP on all local virtual desktops.
    The KDE project KRDC worked well, but pulled in many additional dependencies.

*Why the name &#8220;Remmina&#8221;?*

    Mostly personal preference, but can also stand for "remote mini assistant".

### Usage {#faq---usage}

*Where are all Remmina connections and preferences stored?*

    In two hidden folders, <code>$HOME/.config/remmina</code> and <code>$HOME/.local/share/remmina</code>.

*Can a connection be started using a using a shell command?*

    Yes, use <code>remmina -c filename</code>.

*How are Remmina passwords stored? Are they secure?*

    They are encrypted using 3DES with a 256 bit randomly generated key. Keep your key secure.
    If you use kwallet, gnome-keyring or a similar solution, your password will be managed automatically by libsecret, which is
    superior to the aforementioned native Remmina encryption. A future solution of password and file encryption with libsodium is being implemented.

*I still have questions‽*

    Consult the [https://gitlab.com/Remmina/Remmina/wikis/home](Remmina wiki page){:target="_blank"} or ask on IRC (#remmina on freenode.