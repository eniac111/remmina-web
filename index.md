---
title: Remote desktop client with RDP, SSH, SPICE, VNC, and X2Go protocol support.
layout: splash
excerpt: "Remmina — X2Go, RDP, SSH, SPICE, VNC, and X2Go protocol remote desktop client."
intro:
  - excerpt: ❗ **[Remmina is looking for new maintainers](https://remmina.org/looking-for-maintainers/)**. ❗
      

      Work from home or access files remotely.👾
      

      Not only free as in gratis and Open Source, but also copyleft.🐋
      

      Lovingly made, funded, and translated by the likes of you.🐺
      

      Available in 67 languages and 50+ distributions.🦔
feature_row:
  - image_path: /assets/images/screenshots/Remmina_quickhowto.jpg
    image_caption: "[YouTube video](https://www.youtube.com/watch?v=5DQ3E-W6z-I)"
    alt: "Remmina installation"
    title: "Installation"
    excerpt: "Guides for your Linux distribution."
    url: "/how-to-install-remmina/"
    #btn_label: "…"
    btn_class: "btn--primary"
  - image_path: /assets/images/RemminaUI-1024x576.png
    image_caption: "[Ubuntu screenshot](/assets/images/RemminaUI-1024x576.png)"
    alt: "Features in Remmina"
    title: "Features"
    excerpt: "Plugins, kiosk mode, multi-monitor."
    url: "/remmina-features/"
    #btn_label: "…"
    btn_class: "btn--primary"
  - image_path: /assets/images/meditate-small.jpg
    alt: "Copylefted libre software"
    title: "Copylefted and libre"
    excerpt: "Use, see, change, and share with all."
    url: "/remmina-floss/"
    #btn_label: "…"
    btn_class: "btn--primary"
author_profile: false
classes:
  - landing
  - wide
---


<!-- more -->

# Remote access screen and file sharing to your desktop.

{% include feature_row id="intro" type="center" %}

{% include feature_row %}

### Remote Access Protocol Plugins

<div id="protocols">
<ul>
<li>•&nbsp;<a href="/remmina-rdp/"   >RDP</a>&nbsp;</li>
<li>•&nbsp;<a href="/remmina-ssh/"   >SSH</a>&nbsp;</li>
<li>•&nbsp;<a href="/remmina-spice/" >SPICE</a>&nbsp;</li>
<li>•&nbsp;<a href="/remmina-vnc/"   >VNC</a>&nbsp;</li>
<li>•&nbsp;<a href="/remmina-x2go/"  >X2Go</a>&nbsp;</li>
<li>•&nbsp;<a href="/remmina-www/"   >HTTP/HTTPS</a>&nbsp;</li>
</ul>
</div>

<a href='https://flathub.org/apps/details/org.remmina.Remmina'><img width='180' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-i-en.svg'/></a>       [![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/remmina)

[![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/Remmina/donate) <script async src="https://liberapay.com/Remmina/widgets/button.js"></script> <noscript><a href="https://liberapay.com/Remmina/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript> <img alt="Tally of funds collected per week" src="https://img.shields.io/liberapay/receives/Remmina.svg?logo=liberapay"> <img alt="Liberapay donate button" src="https://img.shields.io/liberapay/patrons/Remmina.svg?logo=liberapay">
[![](https://opencollective.com/remmina/tiers/badge.svg)](https://opencollective.com/remmina)

