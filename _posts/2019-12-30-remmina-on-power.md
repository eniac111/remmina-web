---
title: Remmina on PowerPC
author: Antenore Gatta
layout: single
permalink: /remmina-on-powerpc/
excerpt: The GNU/Linux PowerPC notebook project tested Remmina on a PowerPC
categories:
  - News
tags:
  - powerpc
gallery:
  - url: /assets/images/screenshots/remmina_power_pc.jpg
    image_path: /assets/images/screenshots/remmina_power_pc.jpg
  - url: /assets/images/screenshots/remmina_power_pc_2.jpg
    image_path: /assets/images/screenshots/remmina_power_pc_2.jpg
---

{% include figure image_path="/assets/images/ppc_notebook.jpg" %}

[The GNU/Linux open hardware PowerPC notebook project](https://www.powerpc-notebook.org/en/) aims to produce a PowerPC powered notebook.
Though in its early design stages, [PCB](https://www.powerpc-notebook.org/2019/12/pcb-for-an-happy-new-year/) schematics are already on offer, and a [devkit](https://www.flickr.com/photos/free-software-center/49128563727/) was shown at SFScon19.

PowerPC is one of the most modern processor architectures to date:

* X86-1978
* MIPS-1981
* ARM-1983
* PowerPC-1991

PowerPC is not not encumbered by the limitations backwards compatibility brings, hasn't seen the slew of silicon exploits of for example X86_64 as of late. It is quite efficient in terms of power consumption, making its way into even embedded devices, laptops and high performance computing clusters.

The [open design](https://en.wikipedia.org/wiki/Power_ISA) and its whole modern specification brings a fresh take on the Intel, AMD and ARM monopoly, and now everyone can do something to push it at a step further.

Hopefully this project can be funded to production, and others follow their example. The opposite is a business as usual of predicitive failure.

[Joining](https://www.powerpc-notebook.org/join/) the project is one way, and sending money the way of their [donation campaign](https://www.powerpc-notebook.org/campaigns/donation-campaign-for-pcb-design-of-the-powerpc-notebook-motherboard/) also helps.

Thanks to the Free Software movement, the Linux&#x7c;GNU kernel, all the BSD kernels and most, if not all, of the existing libre software, the laptop facilitates ample choice of good things to run.

The first version will have these specifications:

* Chassis: [Slimbook Eclipse notebook case 15,6”](https://slimbook.es/en/noticias-notas-de-prensa-y-reviews/435-slimbook-collaborates-with-the-powerpc-laptop)
* CPU: NXP T2080, e6500 64-bit Power Architecture with Altivec technology
* 4 x e6500 dual-threaded cores, low-latency backside 2MB L2 cache, 16GFLOPS x core
* RAM: 2 x RAM slots for DDR3L SO-DIMM
* Video: MXM3 Radeon HD Video Card (removable)
* Audio: C-Media 8828 sound chip, audio-in and audio-out jacks
* USB: 3.0 and 2.0 ports
* NVM Express (NVMe), M.2 2280 connector
* 2 x SATA3
* 1 x SDHC card reader
* 1 x ethernet RJ-45 connector
* Wi-Fi connectivity
* Bluetooth connectivity
* Power: On-board battery charger and power-management

If still finding yourself in need of accessing an antiquated and closed source OS, Remmina can be used to do so, as shown in the next two devkit screenshots.

{% include gallery caption="Remmina on PowerPC" %}

More news of better alternative type devices to follow.
