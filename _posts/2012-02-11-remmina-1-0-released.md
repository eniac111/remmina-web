---
title: Remmina 1.0 released!
date: 2012-02-11T08:50:00+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-1-0-released/
excerpt: Historical Remmina blog article
categories:
  - announcement
  - News
tags:
  - release
---
The first release since Remmina was moved from SourceForge to the FreeRDP GitHub repository as a sister project. Released to cooperate with the new FreeRDP 1.0, with a lots of help from the FreeRDP community.
