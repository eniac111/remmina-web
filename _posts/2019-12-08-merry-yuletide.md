---
title: What did you get for Yule this year?
date: 2019-12-08T08:41:00+00:00
author: Allan Nordhøy
layout: single
permalink: /what-did-you-get-for-yule-this-year/
excerpt: 1.3.7 is released, and funding.
categories:
  - News
  - announcement
  - Release
  - Fundraiser
tags:
  - release
---

You just got a new Remmina, with all new just about everything.
Homemade, packaged nicely, with every intention of fitting your every need.
We hope you are otherwise cozy, warm, happy, and healthy.

As the seasons turn, and we are fortunate enough to be alive, you can even keep adding to the gift of giving.
Find someone you like, even yourself, and [gift that special someone a donation for Remmina](https://remmina.org/donations/).

We will even include a personalized 'thank you', or -message to your cherished recipient.

The money will be used for hosting, storage for a new feature, and Weblate hosting, providing even more features for the new [Remmina translation effort](https://hosted.weblate.org/engage/remmina/)

The [full changelog](https://gitlab.com/Remmina/Remmina/-/releases#v1.3.7) for 1.3.7 keeps the ground elves at bay.

Merry Yuletide.