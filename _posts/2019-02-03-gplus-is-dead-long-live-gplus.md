---
title: G+ is dead, good riddance G+
author: Antenore Gatta
layout: single
permalink: /gplus-is-dead/
excerpt: G+ is going to be decomissioned, we propose alternatives
categories:
  - News
tags:
  - community
  - gplus
---

It has been announced and it’s definitive, G+ is going to be shut down in April.

The best thing to do is finding an alternative together.

As promised we have compiled a list of possible options you can vote on. If you don’t like any of our picks, you can specify your own choice.

<script type="text/javascript" async>
(function(d, s, id){
  var js,
      fjs = d.getElementsByTagName(s)[0],
      r = Math.floor(new Date().getTime() / 1000000);
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id; js.async=1;
  js.src = "https://www.opinionstage.com/assets/loader.js?" + r;
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'os-widget-jssdk'));
</script><div class="os_poll" data-path="/polls/2537334" id="os-widget-2537334"></div>
