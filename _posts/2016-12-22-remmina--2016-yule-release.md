---
title: Remmina 2016 Yule Release
date: 2016-12-22T10:13:31+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-2016-yule-release/
excerpt: Remmina release announcement
twitter_share:
  - 'a:1:{s:8:"hashtags";a:1:{i:0;s:40:"remmina #rdp #vnc #remote-desktop #floss";}}'
categories:
  - Release
tags:
  - release
  - Remmina
---

Changelog
=========

[v1.2.0-rcgit.17](https://github.com/FreeRDP/Remmina/tree/v1.2.0-rcgit.17) (2016-12-22)
---------------------------------------------------------------------------------------

[Full
changelog](https://github.com/FreeRDP/Remmina/compare/v1.2.0-rcgit.16...v1.2.0-rcgit.17)

**Implemented enhancements:**

-   Remove survey as too bloated.
    [\#1064](https://gitlab.com/Remmina/Remmina/-/issues/1064)
-   Cannot connect to neoma-bs RDP server
    [\#1056](https://gitlab.com/Remmina/Remmina/-/issues/1056)
-   Import gateay values from RDP files --
    [\#1056](https://gitlab.com/Remmina/Remmina/-/issues/1056)
    [\#1068](https://gitlab.com/Remmina/Remmina/-/merge_requests/1068)
    ([antenore](https://github.com/antenore))
-   No Debian/Ubuntu distributions files included.
    [\#1062](https://gitlab.com/Remmina/Remmina/-/merge_requests/1062)
    ([nastasi](Matteo%20Nastasi))
-   Fix fullscreen window position
    ([\#873](https://gitlab.com/Remmina/Remmina/-/issues/873)
    [\#1060](https://gitlab.com/Remmina/Remmina/-/merge_requests/1060)
    ([spasche](https://github.com/spasche))

**Fixed bugs:**

-   When "Server" GtkComboBox is selected, Tab key doesn't work
    [\#1049](https://gitlab.com/Remmina/Remmina/-/issues/1049)

**Closed issues:**

-   Advanced Settings for RDP connections
    [\#1043](https://gitlab.com/Remmina/Remmina/-/issues/1043)
-   error: 'rdpGdi {aka struct rdp\_gdi}' has no member named
    'bytesPerPixel'
    [\#1028](https://gitlab.com/Remmina/Remmina/-/issues/1028)
-   Remmina crashes attempting VNC connection to Mac OS X Yosemite
    [\#517](https://gitlab.com/Remmina/Remmina/-/issues/517)
-   Remmina can no longer recognize RDP authentication failure
    [\#507](https://gitlab.com/Remmina/Remmina/-/issues/507)
-   SSH-Tunneled VNC connection randomly hangs
    [\#480](https://gitlab.com/Remmina/Remmina/-/issues/480)
-   Long connection times when forwarding RDP connections through SSH
    [\#452](https://gitlab.com/Remmina/Remmina/-/issues/452)
-   Issue with PPA mentioned in wiki
    [\#439](https://gitlab.com/Remmina/Remmina/-/issues/439)
-   Remmina crashes copying from remoted computer and pasting into
    remoting one
    [\#411](https://gitlab.com/Remmina/Remmina/-/issues/411)
-   Font smoothing -- some fonts are not smoothed
    [\#382](https://gitlab.com/Remmina/Remmina/-/issues/382)
-   Remmina blocks and I have to disconnect every 20 minutes
    [\#332](https://gitlab.com/Remmina/Remmina/-/issues/332)
-   Keyboard Mapping
    [\#261](https://gitlab.com/Remmina/Remmina/-/issues/261)
-   Remmina crash when running towards server with xrdp 0.7.0
    [\#234](https://gitlab.com/Remmina/Remmina/-/issues/234)
-   Resolution of Client Viewport not functioning correct
    [\#205](https://gitlab.com/Remmina/Remmina/-/issues/205)

**Merged pull requests:**

-   Updated Russian translations
    [\#1070](https://gitlab.com/Remmina/Remmina/-/merge_requests/1070)
    ([antenore](https://github.com/antenore))
-   Libfreerdp updates
    [\#1067](https://gitlab.com/Remmina/Remmina/-/merge_requests/1067)
    ([giox069](https://github.com/giox069))
-   Removed survey to clean up the code -- API CHANGE
    [\#1065](https://gitlab.com/Remmina/Remmina/-/merge_requests/1065)
    ([antenore](https://github.com/antenore))
-   Updated German translation
    [\#1063](https://gitlab.com/Remmina/Remmina/-/merge_requests/1063)
    ([theraser](https://github.com/theraser))
-   When 'Server' GtkComboBox is selected, TAB doesn't work
    [\#1050](https://gitlab.com/Remmina/Remmina/-/merge_requests/1050)
    ([antenore](https://github.com/antenore))
-   Customizable app name and locations
    [\#1046](https://gitlab.com/Remmina/Remmina/-/merge_requests/1046)
    ([3v1n0](https://github.com/3v1n0))
-   remmina.desktop: add \"Quit\" desktop action for Unity
    [\#1045](https://gitlab.com/Remmina/Remmina/-/merge_requests/1045)
    ([3v1n0](https://github.com/3v1n0))
-   Update Spanish Translation
    [\#1044](https://gitlab.com/Remmina/Remmina/-/merge_requests/1044)
    ([jgjimenez](https://github.com/jgjimenez))
-   Update French translation
    [\#1038](https://gitlab.com/Remmina/Remmina/-/merge_requests/1038)
    ([DevDef](https://github.com/DevDef))
-   Update Uzbek cyrillic translation
    [\#1037](https://gitlab.com/Remmina/Remmina/-/merge_requests/1037)
    ([ozbek](https://github.com/ozbek))
