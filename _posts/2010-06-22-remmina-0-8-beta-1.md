---
title: Remmina 0.8 beta 1
date: 2010-06-22T08:47:00+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-0-8-beta-1/
excerpt: Historical Remmina blog article
categories:
  - announcement
  - News
tags:
  - release
---
Switching to <a href="https://www.freerdp.com/" target="_blank">FreeRDP</a> is one of the biggest changes in the 0.8 release. The plugin system is another big change. Plugins are released in a separated source package, and each plugin is intended to be separated in its own binary package, so that dependencies can be properly set. Complete release notes will be announced after 0.8 becomes stable.
