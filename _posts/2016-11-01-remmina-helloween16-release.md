---
title: Remmina Helloween16 Release
date: 2016-11-01T01:22:54+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-helloween16-release/
excerpt: Remmina release announcement
categories:
  - Release
tags:
  - release
  - remmina
---

**Implemented enhancements:**

* Add RDP scaling factor as implemented by FreeRDP [#969](https://gitlab.com/Remmina/Remmina/-/issues/969)
* Please add man pages for Remmina. [#952](https://gitlab.com/Remmina/Remmina/-/issues/952)
* Please support XDG base directory spec [#818](https://gitlab.com/Remmina/Remmina/-/issues/818)
* Auto-highlight quick search text upon each remote session launch [#544](https://gitlab.com/Remmina/Remmina/-/issues/544)
* SPICE support [#117](https://gitlab.com/Remmina/Remmina/-/issues/117)
* CMAKE: Fix GNUInstallDirs usage [#1004](https://gitlab.com/Remmina/Remmina/-/merge_requests/1004) ([hasufell](https://github.com/hasufell))
* XDG base directory spec [#1003](https://gitlab.com/Remmina/Remmina/-/merge_requests/1003) ([antenore](https://github.com/antenore))
* When focus-in inside the quick search, select the whole text as per #544 [#1001](https://gitlab.com/Remmina/Remmina/-/merge_requests/1001) ([antenore](https://github.com/antenore))
* SPICE: Send a notification at the end of file transfers [#994](https://gitlab.com/Remmina/Remmina/-/merge_requests/994) ([larchunix](https://github.com/larchunix))
* SPICE: Show progress of drag & drop file transfers [#993](https://gitlab.com/Remmina/Remmina/-/merge_requests/993) ([larchunix](https://github.com/larchunix))

**Fixed bugs:**

* SSH &#8211; Secure Shell Issue [#936](https://gitlab.com/Remmina/Remmina/-/issues/936)
* Auto-highlighted &#8220;Quick Connect&#8221; connection name within new connection dialog copies the words &#8220;Quick Connect&#8221; into PRIMARY selection [#263](https://gitlab.com/Remmina/Remmina/-/issues/263)
* Can&#8217;t connect to 2008r2 when set &#8220;negotiation&#8221; security mode [#202](https://gitlab.com/Remmina/Remmina/-/issues/202)
* Remmina crash when terminal bell rings [#163](https://gitlab.com/Remmina/Remmina/-/issues/163)
* Support for left-handed mouse [#136](https://gitlab.com/Remmina/Remmina/-/issues/136)

**Closed issues:**

* Segmentation error on Gnome desktop with Wayland [#1034](https://gitlab.com/Remmina/Remmina/-/issues/1034)
* Untranslated lines missed [#1029](https://gitlab.com/Remmina/Remmina/-/issues/1029)
* Security Issue? [#1027](https://gitlab.com/Remmina/Remmina/-/issues/1027)
* Remmina fails to compile against FreeRDP [#1015](https://gitlab.com/Remmina/Remmina/-/issues/1015)
* VNC connection doesn&#8217;t work if color is set to 256 colors (8 bpp) [#989](https://gitlab.com/Remmina/Remmina/-/issues/989)
* Remmina.org down [#988](https://gitlab.com/Remmina/Remmina/-/issues/988)
* Random Remmina exits (no crash, just plain close) [#978](https://gitlab.com/Remmina/Remmina/-/issues/978)
* SPICE plugin missing on Ubuntu PPA [#958](https://gitlab.com/Remmina/Remmina/-/issues/958)
* Arch Linux: Unable to load RDP plugin [#931](https://gitlab.com/Remmina/Remmina/-/issues/931)
* Non-working copy&paste between KeePass 2 and Remmina [#900](https://gitlab.com/Remmina/Remmina/-/issues/900)
* Can&#8217;t connect to certain hosts after update [#855](https://gitlab.com/Remmina/Remmina/-/issues/855)
* Huge icons in toolbar in some desktop environments [#826](https://gitlab.com/Remmina/Remmina/-/issues/826)
* Regression in RDP plugin &#8211; no IPv6 support [#528](https://gitlab.com/Remmina/Remmina/-/issues/528)
* RDP on custom port [#237](https://gitlab.com/Remmina/Remmina/-/issues/237)
* Provide a different icon for the systray icon [#225](https://gitlab.com/Remmina/Remmina/-/issues/225)
* Segfault after login by RDP [#200](https://gitlab.com/Remmina/Remmina/-/issues/200)
* Remmina in fullscreen does hide Panels on other unused monitors in GNOME [#188](https://gitlab.com/Remmina/Remmina/-/issues/188)
* Regression: Connections scrolling dissapeared [#185](https://gitlab.com/Remmina/Remmina/-/issues/185)
* Remmina hangs/Stucks at second login to RD Server [#179](https://gitlab.com/Remmina/Remmina/-/issues/179)
* Can't see the Windows login screen while RDPing to a machine [#174](https://gitlab.com/Remmina/Remmina/-/issues/174)
* LIBVNCSERVER\_WITH\_CLIENT_TLS is never defined [#173](https://gitlab.com/Remmina/Remmina/-/issues/173)
* Cannot connect to VNC servers using IPv6 [#170](https://gitlab.com/Remmina/Remmina/-/issues/170)
* Connection window disappears when minimized [#155](https://gitlab.com/Remmina/Remmina/-/issues/155)
* Build fails with -DWITH_GETTEXT=OFF [#142](https://gitlab.com/Remmina/Remmina/-/issues/142)
* Remmina RDP session freezes or hides [#137](https://gitlab.com/Remmina/Remmina/-/issues/137)
* Cut&paste [#106](https://gitlab.com/Remmina/Remmina/-/issues/106)
* Vertical resolution of RDP host not being determined correctly [#81](https://gitlab.com/Remmina/Remmina/-/issues/81)
* Regression: In remmina 1.0 systray menu right click editing broken [#61](https://gitlab.com/Remmina/Remmina/-/issues/61)
* Do SSH server host key check with the NX plugin [#18](https://gitlab.com/Remmina/Remmina/-/issues/18)

**Merged pull requests:**

* Fixes workspace detection outside X11 backend. [#1036](https://gitlab.com/Remmina/Remmina/-/merge_requests/1036) ([giox069](https://github.com/giox069))
* Man pages as per #952 request [#1033](https://gitlab.com/Remmina/Remmina/-/merge_requests/1033) ([antenore](https://github.com/antenore))
* Updated Hungarian translation [#1032](https://gitlab.com/Remmina/Remmina/-/merge_requests/1032) @meskobalazs
* updated German translation [#1031](https://gitlab.com/Remmina/Remmina/-/merge_requests/1031) ([morph027](https://github.com/morph027))
* New gettext strings, closes #1029 [#1030](https://gitlab.com/Remmina/Remmina/-/merge_requests/1030) ([antenore](https://github.com/antenore))
* Improves focus detection when keyboard is captured [#1025](https://gitlab.com/Remmina/Remmina/-/merge_requests/1025) ([giox069](https://github.com/giox069))
* Show information on upgrade libssh to 0.7.X [#1017](https://gitlab.com/Remmina/Remmina/-/merge_requests/1017) ([e-alfred](https://github.com/e-alfred))
* Update French translation [#1014](https://gitlab.com/Remmina/Remmina/-/merge_requests/1014) ([DevDef](https://github.com/DevDef))
* Implementing RDP remote scaling and orientation [#979](https://gitlab.com/Remmina/Remmina/-/merge_requests/979) ([giox069](https://github.com/giox069))
* Grab focus without selecting text in the remmina file editor [#976](https://gitlab.com/Remmina/Remmina/-/merge_requests/976) ([antenore](https://github.com/antenore))
* Make gettext *really* behave correctly [#972](https://gitlab.com/Remmina/Remmina/-/merge_requests/972) @diogocp
* Update name [#971](https://gitlab.com/Remmina/Remmina/-/merge_requests/971) ([qwertos](https://github.com/qwertos))

