---
title: Remmina 0.7.1 released!
date: 2009-12-22T08:42:00+00:00
author: Antenore Gatta
layout: single
excerpt: Historical Remmina blog article
permalink: /remmina-0-7-1-released/
categories:
  - announcement
  - News
tags:
  - release
categories:
---
This is a bugfix release of the 0.7 branch of Remmina.
