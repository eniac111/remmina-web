---
title: Remmina SPICED has been released
date: 2016-05-30T10:59:57+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-spiced-has-been-released/
excerpt: Remmina release announcement
categories:
  - Release
tags:
  - Mir
  - release
  - Remmina
  - SPICE
  - Wayland
---
A new amazing release bringing you some fantastic
new features and improvements. As an image is often better than 1000
words, here it is:

![Remmina new main window](/assets/images/Selection_153.png)

Main enhancements:
- [Wayland](https://wayland.freedesktop.org/)
support (brought to you by Giovanni Panozzo).
- SPICE protocol plugin
(mainly used by KVM) (brought to you by Denis Ollier).
- Remmina main refactoring (brought to you by Antenore Gatta & Giovanni Panozzo) A big
thanks to Giovanni, Dennis and Antenore for the coding, to Johannes and
our G+ community for their invaluable support.

Change Log
==========

[1.2.0.rcgit.13](https://github.com/FreeRDP/Remmina/tree/1.2.0.rcgit.13) (2016-05-30)
-------------------------------------------------------------------------------------

[Full
Changelog](https://github.com/FreeRDP/Remmina/compare/1.2.0.rcgit.12...1.2.0.rcgit.13)

**Implemented enhancements:**

-   Move Focus to Quick\_Find search-box Upon Typing
    [\#837](https://gitlab.com/Remmina/Remmina/-/issues/837)
-   RDP: Fix extended mouse event to register the click for forward/back
    buttons [\#638](https://gitlab.com/Remmina/Remmina/-/issues/638)
-   Remmina main window refactoring
    [\#875](https://gitlab.com/Remmina/Remmina/-/merge_requests/875)
    ([antenore](https://github.com/antenore))

**Fixed bugs:**

-   \[remmina\_main\] Quick connect bar not visible
    [\#878](https://gitlab.com/Remmina/Remmina/-/issues/878)
-   Remmina doesn't support ssh-rsa hostkeys
    [\#647](https://gitlab.com/Remmina/Remmina/-/issues/647)
-   Toolbar buttons initially enabled in the main window
    [\#467](https://gitlab.com/Remmina/Remmina/-/issues/467)

**Closed issues:**

-   Only access via administrator account (Raspbian → Windows 10)
    [\#880](https://gitlab.com/Remmina/Remmina/-/issues/880)
-   Location of saved connections is wrong in wiki
    [\#866](https://gitlab.com/Remmina/Remmina/-/issues/866)
-   Grayed buttons if no entry or if none selected
    [\#488](https://gitlab.com/Remmina/Remmina/-/issues/488)

**Merged pull requests:**

-   Spice plugin: Smartcard redirection support
    [\#882](https://gitlab.com/Remmina/Remmina/-/merge_requests/882)
    ([larchunix](https://github.com/larchunix))
-   SPICE plugin: USB redirection support + minor fixes
    [\#881](https://gitlab.com/Remmina/Remmina/-/merge_requests/881)
    ([larchunix](https://github.com/larchunix))
-   Spice plugin: Scaling support
    [\#879](https://gitlab.com/Remmina/Remmina/-/merge_requests/879)
    ([larchunix](https://github.com/larchunix))
-   SPICE plugin improvements (again)
    [\#877](https://gitlab.com/Remmina/Remmina/-/merge_requests/877)
    ([larchunix](https://github.com/larchunix))
-   More SPICE plugin improvements
    [\#874](https://gitlab.com/Remmina/Remmina/-/merge_requests/874)
    ([larchunix](https://github.com/larchunix))
-   SPICE plugin improvements
    [\#872](https://gitlab.com/Remmina/Remmina/-/merge_requests/872)
    ([larchunix](https://github.com/larchunix))
-   Initial support for Wayland and Mir
    [\#871](https://gitlab.com/Remmina/Remmina/-/merge_requests/871)
    ([giox069](https://github.com/giox069))
-   New plugin with basic support for the SPICE protocol
    [\#870](https://gitlab.com/Remmina/Remmina/-/merge_requests/870)
    ([larchunix](https://github.com/larchunix))

## Downloads

-   [**Source code**
    (zip)](https://github.com/FreeRDP/Remmina/archive/1.2.0.rcgit.13.zip)
-   [**Source code**
    (tar.gz)](https://github.com/FreeRDP/Remmina/archive/1.2.0.rcgit.13.tar.gz)
