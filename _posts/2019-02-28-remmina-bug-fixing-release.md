---
title: Remmina 1.3.3 bugfix release
author: Antenore Gatta
layout: single
permalink: /remmina-1-3-3-bugfix-release/
excerpt: Remmina release announcement
categories:
  - announcement
  - News
tags:
  - release
---

This is, again, hopefully the last bugfix following [v1.3.0](/remmina-sandwich-release/).

As usual, thanks a lot to those promptly reporting issues.

The past 10 days:

## Changelog

## [v1.3.3](https://gitlab.com/Remmina/Remmina/-/tags/v1.3.3)

* Improved Danish translation [!1804](https://gitlab.com/Remmina/Remmina/merge_requests/1804) *@scootergrisen*
* Improved German translation [!1789](https://gitlab.com/Remmina/Remmina/merge_requests/1789) *@OzzieIsaacs*
* Improved Italian translation [!1793](https://gitlab.com/Remmina/Remmina/merge_requests/1793) *@antenore*
* Improved French translation [!1795](https://gitlab.com/Remmina/Remmina/merge_requests/1795) *@DevDef*
* Improved French translation [!1802](https://gitlab.com/Remmina/Remmina/merge_requests/1802) *@DevDef*
* Improved German Translation [!1803](https://gitlab.com/Remmina/Remmina/merge_requests/1803) *@jweberhofer*
* Improved Simplified Chinese [!1792](https://gitlab.com/Remmina/Remmina/merge_requests/1792) *Zheng Qian*
* Improved Turkish translation [!1799](https://gitlab.com/Remmina/Remmina/merge_requests/1799) *@TeknoMobil*

* Fix “Utranslated” typo + XHTML 1.0 strictness + move div CSS rule in style block [!1785](https://gitlab.com/Remmina/Remmina/merge_requests/1785) *@DevDef*
* Revert autoclosed `<script>` tags. It seems to be badly surported [!1786](https://gitlab.com/Remmina/Remmina/merge_requests/1786) *@DevDef*
* Remmina connection window refactoring [!1787](https://gitlab.com/Remmina/Remmina/merge_requests/1787) *@antenore*
* Adding serial and parallel port sharing [!1788](https://gitlab.com/Remmina/Remmina/merge_requests/1788) *@antenore*
* Translation of the new strings [!1790](https://gitlab.com/Remmina/Remmina/merge_requests/1790) *@DevDef*
* RemminaMain window refactoring - Removing deprecated functions. [!1794](https://gitlab.com/Remmina/Remmina/merge_requests/1794) *@antenore*
* Fix #1836 implementing the correct message panel when authenticating [!1796](https://gitlab.com/Remmina/Remmina/merge_requests/1796) *@antenore*
* Make SSH tunnel pwd user manageable and public key import [!1798](https://gitlab.com/Remmina/Remmina/merge_requests/1798) *@giox069*
* Translate 3 new strings [!1800](https://gitlab.com/Remmina/Remmina/merge_requests/1800) *@DevDef*
* Vnci fixes [!1801](https://gitlab.com/Remmina/Remmina/merge_requests/1801) *@antenore*
* Fix Yes/No inversion [!1805](https://gitlab.com/Remmina/Remmina/merge_requests/1805) *@DevDef*
