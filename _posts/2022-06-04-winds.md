---
title: Warm winds
author: Allan Nordhøy
layout: single
permalink: /winds/
excerpt: 1.4.27 released.
categories:
  - News
  - Announcement
  - Release
tags:
  - release
---
Summer solstice is eclipsed by a nice set of changes visible and not. \
Familiar schemes are joined by new colours in the form of SSH colour palettes. \
What isn't fixed by grammar (and what problems aren't?) happen upon the \
lands as Lebensgefahr keeps it on track for terminal gnomes, and XFCE4s alike. \
Wolfgang unsticks Alt keys stuck since Remmina 1.4.3. \
Pointers referencing widgets that are no longer are also freed. \
Giox strengthens file_set_string(), and antenore applies the
refactor 50 for added protection.

The [full salad bowl](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.27) joins a table of leisurely days.

Turns out the winds were friendly all along, \
whisking away excess heat with cool sea air.
