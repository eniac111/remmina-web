---
title: Remmina 0.9.2 released!
date: 2010-12-25T08:50:00+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-0-9-2-released/
excerpt: Historical Remmina blog article
categories:
  - announcement
  - News
tags:
  - release
---
This marks two years of development since the first public release! A release is made in celebrattion. 🙂 Merry yuletide!
