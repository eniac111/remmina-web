---
title: Remmina Smarties Edition
date: 2018-11-15 09:50:40 +0100
author: Antenore Gatta
layout: single
permalink: /remmina-smarties-edition/
excerpt: Remmina release announcement
categories:
  - announcement
  - News
tags:
  - release
---

{% include figure image_path="/assets/images/smarties.jpg" %}

Hello Remminers.

We have just released a new bugfix release.

The changelog is quite small this time, but releasing a new version was needed to fix an issue with X11 forwarding.


- Add desktop-gnome-platform and fix themes in SNAP, fixes issue #1730 and fixes missing SNAP localization (Giovanni Panozzo)
- Fix SNAP icon (Giovanni Panozzo)
- Flatpak: update libssh from 0.8.2 to 0.8.3 (Denis Ollier)
- Flatpak: update libssh from 0.8.3 to 0.8.4 (Denis Ollier)
- Flatpak: update libssh from 0.8.4 to 0.8.5 (Denis Ollier)
- Flatpak: update lz4 from 1.8.2 to 1.8.3 (Denis Ollier)
- Flatpak: update pyparsing from 2.2.0 to 2.2.1 (Denis Ollier)
- Flatpak: update pyparsing from 2.2.1 to 2.2.2 (Denis Ollier)
- Flatpak: update pyparsing from 2.2.2 to 2.3.0 (Denis Ollier)
- Implement smartcard name setting. Should fix #1737 (Antenore Gatta)
- man+help: elaborate on file types of -connect and -edit cmd line options (Mikkel Kirkgaard Nielsen)
- RDP: add FREERDP_ERROR_SERVER_DENIED_CONNECTION message (Giovanni Panozzo)
- Removing X11Forwarding code as it is wrong and causing issues (Antenore Gatta)
- Improved Turkish translation (Serdar Sağlam)

As usual you can follow our [wiki](https://gitlab.com/Remmina/Remmina/wikis/home) to upgrade Remmina to this latest version.

