---
title: PPA is kill
author: Allan Nordhøy
layout: single
permalink: /oh-flatpak/
excerpt: The PPA is kill.
categories:
  - News
  - Announcement
  - Release
tags:
  - release
---

Snippity snappity, get off the hype-train happily.

PPA is on the way out. After a long run it got the figurative Roman emperor's thumbs-down.
"But why"? Such is the very question PPA's pose, only not to answer.

Why can't there just be one libre software operating system, and one always up-to-date distribution that everyone can use? Turns out these are mutually incompatible variables.

Ubuntu is a distribution many newcomers use, and to that effect tries to make things a bit easy. Easier still is [elementaryOS](https://elementary.io), adding its own atop what is most often a one release old version of Ubuntu. Ubuntu does this too, only with a not-yet-realeased Debian as a base. Packaging software for upstream Debian means all those will have the latest (or very outdated) version by the time it tricles down. Look to find something that corrects for this, and you will find everything has a smaller or greater percentage of "outdated" programs. What are the options, and why are there options?

Debian is a distribution with infrequent and seldom timely releases. Ubuntu fixes this problem by being
there every April, as it is there in October. This gives packagers, maintaners, bloggers and others a release cadence to target. Seldom enough to re-think things, and often enough to bring about change. A fix, or a problem. Fortunately there are fixes for problems.

Ever since Ubuntu of old got to be the Ubuntu of new, rolling-release distributions have meanwhile gotten better. Those are distributions adding onto whatever is there, every new package incrementally. This means no migrations between different platforms of changes. Depending on the intended user, rolling releases can be anything from bleeding-edge fresh to now also include the dull reality of a dependable system. With most machines connected to the Internet, they are a more convenient target; for packagers and attackers alike. It brings the distant closer, to the mistake of blogging our food, or those out there doing so.

Things are faster in 2020, the current year; century of Linux on the desktop. Faster better, or faster worse? Both!? For as much is fixed, the only true positive is knowing how much was broken as someone rushed to fix it. Hardware, distributions and programs alike. Gone are the years between releases of anything. All of a sudden you can't count on release numbers ending in even numbers to be more stable than odd ones. The derived numerology bodes well for the soon to be released Remmina 1.4.8.

For distributions that release a "finished" product every now and then, there are smaller point releases to get you by, and the most compelling point is knowing what software is on a system. A need in part arising from having many different releases… No matter what separates them, in even the most stable of distribution releases, the release and accompanying "media" will be outdated quick. The once spacious "CD" could fit a whole distribution and be relevant for a while, but is now in the process of being removed from the translations of the Debian Handbook. The divine floppy disk no longer fits [TempleOS](https://templeos.org/). False temples have been erected.

None more so than Ubuntu LTS, tempting fate by maintaining long-term support releases. Solving one problem as it creates another. You can run Ubuntu 16.04 (est. 20_16_ from the fourth month) [into 2024](https://en.wikipedia.org/wiki/Ubuntu_version_history#Version_timeline)! Out from the ruins they come, carrying software time forgot. Currently the latest Remmina is only on the soon-to-be-released Ubuntu 20.10, with freshness tapering down from there.
Are the masses even ready for the latest false god of shiny new releases?

The latest Remmina version is always the one with the most unknown bugs, but also the version that has less of the stuff known to have been fixed, whereas old versions have more testing. That being said the newer is always the one to be replaced, even as the case may be that the newer version happens to not be as good as it was supposed to. The worse the release the quicker the cadence. Or spend time operating different versions intended for different users. Both schools of thought are valid, but the quicker one wins sloppily.

So what kind of user does the distribution and software maintainer have in mind? If there is a specific software maintainer things are looking up. If not, the old and crufty idea is that the oldest possible version has the most testing, so it _should_ be the most stable, and that only security fixes, if any, should reach it. Debian has the backports repository for this, and Ubuntu has https://help.ubuntu.com/community/PinningHowto

The Linux kernel takes the more conservative approach, for good reason, and it doesn't break userspace between versions for even better reason. Doing so is a world of pain, but then again most software isn't quite the Linux kernel. Nor is Ubuntu, or Remmina, but the former uses the model, so the pain is passed on right down to the user, where things break against old system libraries, or just don't get updated. Remmina releases often, so if everything is packaged, it still works. This means more work for packagers. If you want developers to do it all, now there is no time left to develop, and nothing to release. There is a certain level of trust put on packagers for the ease of official repositories, and there is a certain level of untrustworthy stuff and hardship in doing things differently. This is why we can have good things, if we wait or embrace the bad. New thing bad.

In addition to the different distributed released stages of readiness, Debian also has branches of itself called "still in development" "Unstable" "Testing", and then a does freeze which makes it down to "Stable", before retiring its name and number as "oldstable". Debian (Stable) is on number 10 "Buster" right now. You can pull in single packages from one into the other, but are advised from doing so. In any event there are just packages that fall between the cracks still. Remmina has 10 releases not including 1.4.8 so far in 2020 alone, and then FreeRDP has releases, and all the libraries it uses.
Even making it every time on release like [Gentoo](https://www.gentoo.org/) manages to, that is a lot of work.

Do you compile your own as a user? Anyone can make the complete Remmina package from the source code on their machine on any distribution. Curiously this is often harder on a system with older system packages, as newer stuff often requires newer libraries to build against. The sweet-spot is being able to fix something that rarely breaks. That is where a lot of development happens, often by accident. Maybe not everything was better in the hay-day of times past moved into the unwilling future?
To illustrate a point, "The Debian way" used to be the way. In uncharacteristic fashion, Debian thought it would have to do too much work itself, and adopted systemd, which is nothing like the ways of old. Meanwhile Ubuntu had made their own init system, and eventually discarded it. Fortunately the Linux community has your ticket. Choice is good if you make good choices. From the galaxies of every micro-cosmos being one onto itself, we find [Devuan](https://devuan.org/) is what Debian used to be, meaning without systemd, _and_ it has an updated Remmina. If that sounds like the kind of clunky noises you like, fold up your sleeves from there.

If you just want a shortcut to the bliss of updated software, you could install the PPA, if you are on Ubuntu. This is a separate repository maintained often at times by the maker of the software in it. Or you don't. You can even mock the gods by repackaging a [PPA](https://wiki.debian.org/CreatePackageFromPPA) to tarnish your Debian. Turns out those old and gray advisers on things harmful got one right for the ages. Sometimes your PPA is some person that forgets about solving someone else's problem at some point in time, and now the system stuff you got from it messes with your otherwise functional system. Yes, system packages too. Oh dear. Fixing that is often less easy than compiling would have been, so then what?

Meta-packaging that can be removed and installed as per ones every whim? Great, what could possibly go wrong. Oh Flatpak, or Snap. Why the both? Well, Canonical, (makers of Ubuntu) thinks if an error exists, that adopted lesson is better learnt failing in-house. Many a creation has been carried out the door, by the software protection services. _Snap_ is the Canonical variant and _Flatpak_ is sort what Gnome has. Lest we forget Ubuntu was using Gnome, before it made its own everything, scrapped that, and is now Gnome again, but _Snap_. Remmina does both Snap and Flatpak in-house, and is fortunate to have many great packagers keeping things fresh in the various distributions.

**TL;DR**
PPAs exist in Launchpad, which is a sort of bug-reporting, translation, build-system. Canonical isn't putting a roof over its head(s) nowadays. Closing the door on that chapter is pressing, as building the PPA doesn't integrate with the regular GitLab automation. The Remmina PPA was missing functionality too.

Why won't you answer our calls Canonical? We love you Canonical. Don't hang up. We can have a nice little future with updated relea-\*beep\*