---
title: News and More
author: Antenore Gatta
layout: single
permalink: /we-got-news-and-more/
categories:
  - article
---
Testing is underway before the release of 1.3.5.

Here is what is in store:

- Configurable, human readable profile file names by Antenore Gatta

    Save your profiles where you want and with an easier way to identify the filename.

- Many notable and subtle bugs fixed by Giovanni Panozzo.

    Null pointer reference, RDP issues, Remmina connection window fixes and refactoring and many more!!

- Remmina news (and announcements) widget by Antenore Gatta.

    Periodically (sporadically) a widget will be shown with news and announcements related to your Remmina version.

- Master password, to protect settings and profiles from unauthorized modifications by Antenore Gatta

    If you set it in the general preferences, Remmina will ask for a passphrase each time you try to edit, delete, copy a profile or change the general settings.

- WWW plugin (web browser with authentication for Remmina) by Antenore Gatta.

    This is a replacement of the external WebKit Remmaina plugin. The big advantage is you can save your username and password. In the future support for webdriver/selenium and company will be added.

- Preferences cleaning by Antenore Gatta
- Profile saving bug, fixed by Antenore Gatta.
- Remmina main UI improvements by Antenore Gatta.
- Translations by AsciiWolf, Denis Ollier, Davy Defaud and Johannes Weberhofer.
- Typographic and wording corrections by Davy Defaud.
- Flatpak updates to use latest libs (for a better experience) by Denis Ollier.

This time we would really appreciate your help to test, report bugs, improvements, translations, ideas and [offer us a coffee](https://remmina.org/donations/) :)
