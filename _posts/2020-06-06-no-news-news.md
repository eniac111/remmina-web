---
title: No news news
author: Allan Nordhøy
layout: single
permalink: /no-news-news/
excerpt: 1.4.6 released.
categories:
  - News
  - Announcement
  - Release
  - Fundraiser
tags:
  - release
---

[The general issue](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=961501) to solve this time around is that of Remmina contacting the Remmina web server to fetch news.
Doing so automatically has some privacy implications, and it happens to not be in line with the Debian guidelines.
There is now a compile time switch to turn off the functionality entirely, and it is opt-in to begin with.

A company is discarding their older laptops by donating them to a school, installing Debian, and Remmina on them,
helping students tune in to remote lectures. A very good initiative that all businesses should consider.
If you can formulate what your use-case is, and explain how Remmina might be a better fit than it is, the door is open.
So much so you can do it yourself, the interface now has a a section to write notes for each profile and the widgets
in the new behavior tab have been rearranged to save vertical space—the final frontier.

A [script](https://github.com/mkud/Remmina_mRemoteNG_conv) is now available for exporting passwords from mRemoteNG to Remmina.

Hungarian is now a language with full coverage in Remmina. Or spoken in optimistic Hungarian:
“Nem a következő száz év kezdődött el, hanem a munka folytatódik. A magyarokkal eddig bármit megcsinálhattak, következmények nélkül.”
If you long for la terreta, Valencian is in, or you can try it in Catalan.

“L'Ajuntament es reserva les facultats d'inspecció i de control sobre les activitats

En el cas que es detectin incompliments de la normativa vigent o discrepàncies, inexactituds, omissions, etc.
entre el que es declara documentalment i el que es comprova en la realitat del funcionament de l'activitat,
la comunicació s'entendrà que no és vàlida i no pot produir efectes.
En aquest sentit s'haurà de valorar la paralització de l'activitat i es podran adoptar les mesures disciplinàries adients,
en el marc del procediment sancionador que correspongui, sense perjudici d'haver d'abonar la taxa d'inspecció posterior que preveu l'ordenança fiscal vigent.”

Every single person donating big or small to the [Remmina Liberapay](https://liberapay.com/Remmina/) makes us happy,
and it grants Antenore some time away from work to get the multi-monitor functionality ready.
[Other methods](https://remmina.org/donations/) are also available. [GitHub](https://github.com/sponsors/antenore) is currently matching donations to Antenore,
so while not the best option, it does double what you give.

The full changelog [for 1.4.6](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.6) is a cart pulled by the many.

Thank you to and from everyone in this release.