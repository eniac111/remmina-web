---
title: The patrician's choice
author: Allan Nordhøy
layout: single
permalink: /the-patricians-choice/
excerpt: Reform now.
categories:
  - News
tags:
  - Crowdfunder
  - ARM
gallery:
  - url: /assets/images/36c3one.jpg
    image_path: /assets/images/36c3one.jpg
  - url: /assets/images/36c3two.jpg
    image_path: /assets/images/36c3two.jpg
  - url: /assets/images/ThinkPad555BJ.jpg
    image_path: /assets/images/ThinkPad555BJ.jpg
---

Construction, quality, mechanical, tactility.

Spiritually, the [MNT Reform](https://www.crowdsupply.com/mnt/reform) is a ThinkPad
in the way that ThinkPads used to be ThinkPads,
but are no longer, though in name only.

{% include gallery caption="Dragonbox Pyra at 36c3 for size" %}

It is a symphony of productivity.

If Richard Sapper lived to see the day,
he would have wanted to use it.
It is an experience, that appreciation of an object
crafted with a laudable goal in mind. An assembled unity
that makes for a communicative nod between you and its
creator. You need not know who, because if you didn't already know,
now you know why.

If it doesn't matter to you how, where, by whom, and from what
things are made, then change that about yourself.
If not, at least aim to land somewhere between the corporate cobblestones and
the suicide nets they put up. Sometimes one wishes everyone understood
the quality of something made to last; what once was.
You may not be old enough to remember trackballing laptops,
if so at fault of the times we are in.
Trackballs are still quietly superior, and clicky buttons still rule the day.
Well, I guess they just started making 'em like they used to.
You can get one for your children, and it doesn't matter if you havent got any yet.

It doesn't have lies, because it doesn't need
lies. And where does it record your face? Nowhere.
It is made with honesty, and with that in mind;
Proprietary horseshit? No. Just no. Yes. Yes indeed.
What a world it is to be bestowed the manifestation
of every form-factor one could want.
To parttake in it, be it by building your own,
putting together the parts, or taking them apart.

Imagine this, you don't have to deal with
corporate nonsense, because it runs Linux|GNU,
and as a Remmina user, you can close all other nonsense when you are done with it,
and you are. Business is back in business.
You are at home, or where you want to be.
It doesn't matter that ThinkPad quality went to shit.
You have a trackball.
Nothing matters when you have a trackball.

The laptop reborn, from the cold dead hands of lesser men that
tried to get their feeble and weak hands on them.
If some youngin tries to tell you it is not corporate policy to have one,
appreciate its heft as it hits the small-minded individual in the head
from afar. You need not worry, because chances are you can keep working away
without the hinge having moved. Productivity strikes.

What kind of patrician has a trackball on their laptop?
You.
