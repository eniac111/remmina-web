---
title: Remmina "Before The Storm" Edition
date: 2018-10-07T11:15:40+00:00
author: Dario Cavedon
layout: single
permalink: /remmina-before-the-storm-edition/
excerpt: 1.2.32 release announcement
categories:
  - announcement
  - News
tags:
  - release
---

{% include figure image_path="/assets/images/before_the_storm.jpg" %}

Hello Remminers. Toiling away to give you a solid and secure environment, brings a new release of Remmina, with bugfixes and improvements aplenty.

* Allow Wayland backend again when GTK >= 3.22.27 (so annoying!)
* Distinct RPD gateway authentication and other enhancements.
* Added option to honour https_proxy and http_proxy environment variable.
* Custom screenshot filenames.
* Keyboard grabbing improvements.
* UI enhancements.
* Many bugfixes as usual.

A complete changelog can be found on [GitLab](https://gitlab.com/Remmina/Remmina/tags/v1.2.32).

In the next months we are going to "mess up" things as we are working on new features and a huge refactoring,
so this release should be considered a Long Term Support, so don't be surprised if you won't have news from us
for some weeks or even months.
As usual, feel free to open [issues](https://gitlab.com/Remmina/Remmina/issues) or contact us for any kind of problems.
Oh, and if you like it, consider [supporting.](https://remmina.org/donations/)

Image: ["Gett'n Out Of Dodge (County)" by Rick](https://flic.kr/p/eHWoc3)

To install and/or upgrade, follow our [wiki](https://gitlab.com/Remmina/Remmina/wikis/home).
