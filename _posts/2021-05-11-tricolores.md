---
title: Nos drapeaux tricolores
author: Allan Nordhøy
layout: single
permalink: /tricolores/
excerpt: 1.4.14 and 1.4.15 and 1.4.16 released.
categories:
  - News
  - Announcement
  - Release
tags:
  - release
---

Through many changes and questions relating to them, a release culminates in their passing.

liimee for Indonesian and phlostically for French are new faces in translation. \
cth451, xsmile, matir and akallabeth by extention, extended the network of code and its utility to the new and improved. \
raghavgururajan from Guix joins the package in its packaging effort.

Continued donations are very immensely appreciated. We are all helping to bring each-other Remmina.

The [full changelog](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.14) for 1.4.14 is a snapshot in the writings of history brought to account.

Take and do care.

Edit: The release cycle is heating up with this verifiably hot patch.

The [full changelog](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.15) for 1.4.15 brings to bear excitement for what forgetfullness is still in store for next release.

Repeat prior fixes to forgotten mistakes.

Edit2: Or, establish a trifecta of powers to be.
The [full changelog](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.16) for 1.4.16
completes the detour waved on as one cortege of trickles d'oeuvres.

Historical repetition is only temporary forgetfullness.
