---
title: Monitoring multiplicity
author: Allan Nordhøy
layout: single
permalink: /monitoring-multiplicity/
excerpt: 1.4.4 and 1.4.5 released.
categories:
  - News
  - Announcement
  - Release
  - Fundraiser
tags:
  - release
---

In the month it took to make 1.4.4, it saw and 5625 strings changed or
translated on [Hosted Weblate](https://hosted.weblate.org/projects/remmina/remmina/).
Remmina often is the most translated project on Hosted Weblate, which changes
from day to day in the overview on [weblate.org](https://weblate.org).

Polish now has coverage, and is very possible for one person to add a whole language.
"Ziemianin z ziemią ziemniaczaną, tematy szkalujące!"
Now that there are also reviews, quality should improve further.
Most all the strings now have added context, and a screenshot to let you know where it is used.

The work on [improving the UI](https://gitlab.com/Remmina/Remmina/-/issues/2187) continues.
Reworking it to align more with GNOME-aesthetics and concepts was offered, but proposed
under the RedHat umbrella, moved to the GNOME source repository.
A better UI is undoubtably needed, but GNOME doesn't always have the right ideas about things, and
present many changes without sticking to them for long.

Be that as it may, ideas will be gauged on their merit alone, and not how well they align
with any conceptual unity. No level of freedom is lost with that approach.
For Remmina it makes sense to do the least amount of changes, and make it all as simple
as could be for the program, not adhering to a moving target for the sake of it.
If someone wants to work on the [Qt variant of Remmina](https://gitlab.com/Remmina/QRema), that is still on the books.

In the functionality department, short of [workarounds](https://gitlab.com/Remmina/Remmina/-/issues/1997), a [lot of effort](https://gitlab.com/Remmina/Remmina/-/merge_requests/2043) has gone into [multi-monitor support](https://gitlab.com/Remmina/Remmina/-/merge_requests/2013).
It needs another 40 hours of work, optimistically calculated.
The good news is that Antenore has set his sights on getting it done,
but no such developer time is available between working at IBM and everything else.
This means it is only an economical issue, which all in all is the best possible scenario.

The donation effort is going great, and continues seeing increased activity.
We are as motivated by it as able to do more.

To monetise the monitorisation of Remmina, [Remmina is joining Open Collective](https://opencollective.com/remmina),
a platform that helps (mainly) businesses donate, seeing as invoices are on offer for donations.
It however doesn't mean businesses or individuals donating there get an unfair say in matters, rest assured.
In fact funds from the preferable [Remmina Liberapay](https://liberapay.com/Remmina/),
and [other methods](https://remmina.org/donations/) also go toward this effort,
along with the less preferable platforms, or direct ways of sending funds.

The full changelog [for 1.4.4](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.4) is not complete,
but is anyone?

Sadly no solution of releasing [1.4.5](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.5) could be found for vintage Remmina 1.4.4 fans
without also providing functioning keyboard events. New thing bad, indeed.
If you do want be on the latest trendy Remmina, know that 1.4.5 is 0.0.1 better.

Contributions, past and future, and the current; highly appreciated.