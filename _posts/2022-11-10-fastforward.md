---
title: Fast Forward
author: Allan Nordhøy
layout: single
permalink: /fastforward/
excerpt: Granted, it is.
categories:
  - News
  - Announcement
tags:
  - Announcement
  - Opinion
---

The leisurely pace of Remmina takes strides in strident fashion with [Fast Forward](https://www.fastly.com/fast-forward)™ shoes.

Doing things right is somehow antithetical to most of what happens in technology. \
Despite that, it can only seem fitting for Fastly to find it within reason \
to support Remmina with [this grant](https://www.fastly.com/blog/fast-forward-lets-build-the-good-internet-together). \
Thank you.

From the wealth of projects operating on things other than direct profitability, \
it is still a bold move to pick Remmina. \
As now a program within a program, encompassing shared values is what ties \
the room together. No (more) less, no less(oned more)?\
Remmina has no rugs to pull, but it does have the integrity of positioning \
itself with users in mind. \
This is somehow an open crevasse to transpire. \
_For something to remain open, it can't be a door_…

At the other side of it there are overworked individuals spread very thin \
in furnishing the door frame. \
"This is my (temporary) hole in the wall", they say.
For any wall that anyone could and would want to put up for other reasons, \
it is a door that can become more wall, because that key exists. \
The paradoxical ability to be, rather than to remain open pertains to more open \
(for everyone to do anything) only adds the potential of less open for everyone as a
product of lowered entropy.

Openness is only a safety-net as an enshrining cover of effort all the way down, \
rather than a foundation to build openness atop.

Copyleft (however) in any of its forms doesn't guarantee there will be anything to \
remain in place or even exist in and of itself, but in its guaranteed openness it \
must bring down what are then natural and existing obstacles to exist.

Anything not more easily navigated around becomes a wall easier to scale, downwards. \
If that leaves you with nothing left in a downfallen commons, it is however rock-solid. \
Whether it slips through the cracks or welcomes itself in at the door-hole \
is rather irrelevant in the pursuit of lowering the barrier to entry.

If tackling the problems of the world isn't a sustainable venture, those are acceptable terms.
