---
title: E-quality
author: Allan Nordhøy
layout: single
permalink: /e-quality/
excerpt: Relative direction by provenance
categories:
  - News
tags:
  - Poemelemic
  - Opinion
---

Division over what creation takes away is preferred to its eternal net loss.

In much the same way not deriving values from a deity doesn't deprive one of morals, \
omnipotent _accumulardation_ can be found disagreeable \
respective of the powers of individuals involved. \
Opportunistically befitting, the decidedly equititorial \
by way of belt of _redistributution_ at the other side of ends not meeting comes out. \
Is any facet of opportunity more-so represented \
into produced outcomes than its measure of natural distribution?

Some part of ones DNA mitigates its sequencing against our will, \
as having some nuclear power—legitimizes everyone accruing their own in part. \
The middleman at scale answers to your say perhaps, in equal measure short \
of taking at all even given known considerations into account, \
as intrinsically controllable by means not available to you.

Leveled at smoking gun availabilty, the establishmental begats basis of communication \
noisy enough to grant rationational freedom of thought, at effort just \
as gauged by those trying to control it as they see fit.

As produce of prevailing ideas, we think by way of yours \
truly better of ourselves. \
Improved and refined is function at will. \
And won't someone from beyond the fourth wall write better blogposts!?
