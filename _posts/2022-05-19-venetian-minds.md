---
title: 
author: Allan Nordhøy
layout: single
permalink: /venetian/
excerpt: 1.4.26 released.
categories:
  - News
  - Announcement
  - Release
tags:
  - release
---

In summary of events leading up to this occasion, \
buttons were hit in sequences appeasing computers and \
organics alike.

One would be wise to suspend belief in the world of superstition, \
despite the realisation of a Remmina release in the image of great premonition.

This time all the infrastructure is in place for Python plugins to be written. \
Sticking to older guns, forwarding XOrg programs over SSH is possible. \
The kiosk-mode sees improvements to its simple offerings. \
An assorted list of bugs are off the menu entirely.

Many hands on deck, with PHWR, lorenz, marco.fortina, and ToolsDevler also taking part, \
with yet more signing on.

A lot of work went into the Korean, Czech, Finnish, Slovak, Hungarian Brazillian Portuguese, and Japanese \
translations, with other languages also being fixed up for the release.

Everyone funding Remmina receive a lot of thought, but not enough mention. \
Thank you so much to all donators new and old :)

The [full details](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.26) are a summary of happiness.

The fair overshadowing the uncomplicated is a penchant for progress in tradition.
