---
title: Foreshadowing
author: Allan Nordhøy
layout: single
permalink: /foreshadowing/
excerpt: 1.4.11 is released.
categories:
  - News
  - announcement
  - Release
tags:
  - release
---

As a sign of things to come, the waning winter gives promise of embellishing in its wake
all that endured its reign. Remmina looks to gain another winter under its belt with 1.4.11,
safe in the knowledge freezing ones tongue to surrounding beard when out bicycling
is not merely a hypothetical threat-vector.

Multi-factoring up on safety is one of the multi-faceted improvements brought about this time.

Multi-monitorists get a sneaky-peek preview of the monitor-tanning features of the soon-to-come 1.5.0.
Peruse the [dev Flatpak](https://remmina.org/public/packages/remmina-dev.flatpak) at your leisure at any time
to get the outfitted with hottest trends.

[@hadogenes](https://gitlab.com/hadogenes) and [@pnowack](https://gitlab.com/pnowack) landed many fixes this time around,
with Antenore nearing omnipresence.

[@ToolsDevler](https://gitlab.com/ToolsDevler) got preliminary support for Python plugins in.

Almost 1500 strings got translated since the last release; 134 of which were in Kabyle. :)

Catalan 	
    Gil Obradors (24)
    Ecron (37)
Chinese (Simplified) 	
    Eric (64)
Croatian 	
    Milo Ivir (129)
Danish 	
    scootergrisen (28)
English (United Kingdom) 	
    Andi Chandler (26)
French 	
    Davy Defaud (139)
German 	
    Johannes Weberhofer (88)
Greek 	
    Michalis (3)
Hebrew 	
    Yaron Shahrabani (82)
Hungarian 	
    Marczis István (31)
Italian 	
    Fabio Fantoni (1)
    random r (3)
    Roberto Bellingeri (41)
    Antenore Gatta (128)
Japanese 	
    sute1 (14)
    YAMADA Shinichirou (99)
Kabyle 	
    Selyan Sliman Amiri (134)
Kurdish (Central) 	
    Kurd As (38)
Norwegian Bokmål 	
    Allan Nordhøy (22)
Macedonian 	
    Kristijan "Fremen" Velkovski (41)
Portuguese (Brazil) 	
    Mário Victor Ribeiro Silva (21)
    Rafael Fontenelle (26)
Russian 	
    Дубовский Алексей Георгиевич (4)
    Dmitriy Q (8)
Slovak 	
    Dušan Kazik (18)
Swedish 	
    Åke Engelbrektson (2)
Turkish 	
    Oğuz Ersen (3)
    Emin Tufan Çetin (99)
Ukrainian 	
    Yuri Chornoivan (87)

Asiden n umaccaq gar n wammas n wadad n yal tafekka d wagaz-nni yezmer ad yili s usemres n tluft n snat n tfekkiwin!

In short, long changelog is long for 1.4.11.
The full [fanfare](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.11) bodes well for times to come.
