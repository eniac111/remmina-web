---
title: Different how?
layout: single
author_profile: false
permalink: /remmina-floss/
---

Remmina is copylefted libre software.

## What is libre software?

![image]({{ site.baseurl }}/assets/images/gnu-head.jpg){:width="192px" .callin} The ability to use, see, modify, and share–with all.
Requiring sharing on equal terms is what (additionally) makes it copyleft, meaning none of these software freedoms can be taken away.

## You can help develop Remmina

Peruse the [contributing]({{ site.baseurl }}/contributing-to-remmina) page.